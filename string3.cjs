function getMonth(data) {
    let dataValue = data.split("/");
    return dataValue[1];
}

module.exports = getMonth;