function stringConversion(data) {
    if (data.length === 0) {
        return "";
    }
    let stringData = data.reduce((prev, curr) => {
        prev += curr.replace("\"", "") + " ";
        return prev;
    }, "")

    return `"${stringData.trim()}"`;
}

module.exports = stringConversion;