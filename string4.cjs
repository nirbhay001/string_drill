function fullName(data) {
    let nameData = Object.entries(data).reduce((previous, current) => {
        previous.push(current[1]);
        return previous;
    }, []);
    let result = nameData.reduce((previous, current) => {
        let value = (current.charAt(0).toUpperCase() + current.slice(1).toLowerCase())
        previous += value + " ";
        return previous;
    }, "")
    return result;
}
module.exports = fullName;