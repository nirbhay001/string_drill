function ipAdressFormate(data) {
    let result = data.split(".").map((item) => +item);
    if (result.includes(NaN)) {
        return [];
    } else {
        return result;
    }

}
module.exports = ipAdressFormate;
